import Vue from 'vue'
import Router from 'vue-router'
import index from '@/pages/index'
import option2 from '@/pages/option2'
import option3 from '@/pages/option3'
import option4 from '@/pages/option4'

Vue.use(Router)

const router = new Router({
	mode: 'history',
  routes: [
    {
      path: '/',
      name: '/',
      component: index
    },
    {
    	path: '/option2',
      name: 'option2',
      meta:{
      	requireAuth:true,
      },
      component: option2
    },
    {
    	path: '/option3',
      name: 'option3',
      component: option3
    },
    {
    	path: '/option4',
      name: 'option4',
      component: option4
    },
  ]
})

//  判断是否需要登录权限 以及是否登录
router.beforeEach((to, from, next) => {
  if (to.matched.some(res => res.meta.requireAuth)) {// 判断是否需要登录权限
    if (localStorage.getItem('username')) {// 判断是否登录
      next()
    } else {// 没登录则跳转到登录界面
      next({
        path: '/',
        query: {redirect: to.fullPath}
      })
    }
  } else {
    next()
  }
})

export default router