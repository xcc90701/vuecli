import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import router from './router'
// import routerConfig from './router-config'
//引入element-ui
import Element from 'element-ui'
// import 'element-ui/lib/theme-default/index.css'
//引入element-ui默认样式
import 'element-ui/lib/theme-chalk/index.css' 
//引入JQuery
import $ from 'jquery'
//引入axios
import axios from './http'
//修改Vue的原型属性，使用axios
Vue.prototype.$http=axios

Vue.use(Element)
Vue.use(Vuex)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router:router,
  components: { App },
  template: '<App/>'
})
